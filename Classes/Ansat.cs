using System.Linq;
using System.Net;
using System.Reflection.Metadata;
using System;
using System.ComponentModel;
using Bladkompagni.Classes;
using Bladkompagni.Converter;
using static Bladkompagni.Classes.Helpers;
namespace Bladkompagni.Classes
{

	public sealed class Ansat
	{

		public Hexadecimal EmployeeID{get;set;}
		public string Name{get;set;}
		public string Address{get;set;}
		public string POBox{get;set;}
		public string City{get;set;}
		public Enum Position{get;set;}
		public enum Positions {CEO, printer, journalist, marketing, HR};
		public DateTime EmploymentDate{get;private set;}


		// private static volatile IAnsat1 instance;
		private static readonly Ansat syncRootObject = new Ansat();
		public Ansat(){}
		public Ansat(Bladkompagniet bladkompagni, Hexadecimal lastEmployeeID, string name, string address, string pobox, string city, Enum position){
			if(lastEmployeeID.Hex == null && bladkompagni.Employees.Count == 0){
				this.EmployeeID = new Hexadecimal();
				this.EmployeeID.Hex = "0";
			}else{
				this.EmployeeID = HexMath.HexIncrement(lastEmployeeID);
			}
			this.Name = name;
			this.Address = address;
			this.POBox = pobox;
			this.City = city;
			this.Position = position;
			this.EmploymentDate = DateTime.Now;
		}

		public static Ansat Instance
		{
			get
			{
				return Instance;
			}
		}

		public Ansat NewEmployee(Hexadecimal lastEmployeeID, Bladkompagniet bladkompagni){
			Enum position = Positions.printer;
			string name = "";
			string address = "";
			string pobox = "";
			string city = "";
			cc();
			cwl("Ooh! A new employee, huh? ｀;:゛;｀;･(°ε° )");
			bool startover = true;
			do
			{
				cwl("What is their name?");
				cw("→");
				name = crl();
				cwl("Nice! What's their address? (street and house number)");
				cw("→");
				address = crl();
				cwl("What's the PO-Box?");
				cw("→");
				pobox = crl();
				cwl("What city is that?");
				cw("→");
				city = crl();
				cwl($"What position will {name} have?");
				Positions[] positions = (Positions[])Enum.GetValues(typeof(Positions));
				for (int i = 0; i < positions.Length; i++)
				{
					Console.WriteLine($"{i}: {positions[i]}");// I know the limit will be from 0 to 9, but this is good enough for now
				}
				ccc();
				bool correct = false;
				do
				{
					ConsoleKeyInfo keyInfo = crk();
					switch (keyInfo.KeyChar)
					{
						case '0':
							position = Positions.CEO;
							correct = true;
							break;
						case '1':
							position = Positions.printer;
							correct = true;
							break;
						case '2':
							position = Positions.journalist;
							correct = true;
							break;
						case '3':
							position = Positions.marketing;
							correct = true;
							break;
						case '4':
							position = Positions.HR;
							correct = true;
							break;
						default:
							ccc();
							cwl("That's not a valid option. ( ◡‿◡ *)");
							break;
					}
				} while (!correct);
				ccc();
				cwl("Just a checkup, is this correct?");
				cwl($"Name: {name}");
				cwl($"Address: {address}");
				cwl($"PO-Box: {pobox}");
				cwl($"City: {city}");
				cwl($"Position: {position}");
				cwl("Y/N");
				bool breakOut = false;
				do
				{
					ConsoleKeyInfo keyInfo = crk();
					ccc();
					switch (keyInfo.Key)
					{
						case ConsoleKey.Y:
							startover = false;
							breakOut = true;
							break;
						case ConsoleKey.N:
							startover = true;
							breakOut = true;
							cwl("Alright, starting over...");
							zzz(2000);
							ccc();
							break;
						default:
							ccc();
							cwl("I didn't quit catch that.");
							break;
					}
				} while (!breakOut);
				cc();
				cwl($"Awesome! Tell {name} congratulations from me! (o^▽^o)");
				zzz(2000);
			} while (startover);

			Ansat newAnsat = new Ansat(bladkompagni, lastEmployeeID, name, address, pobox, city, position);
			return newAnsat;
		} // End of NewEmployee
	}


	// public class Ansat
	// {
	// 	public Hexadecimal EmployeeID{get;private set;}
	// 	public string Name{get;set;}
	// 	public string Address{get;set;}
	// 	public string POBox{get;set;}
	// 	public string City{get;set;}
	// 	public Enum Position{get;set;}
	// 	public enum Positions {CEO, printer, journalist, marketing, HR};
	// 	public DateTime EmploymentDate{get;private set;}

	// 	public void NewEmployee(Hexadecimal lastEmployeeID){
	// 		Enum position = Positions.printer;
	// 		string name = "";
	// 		string address = "";
	// 		string pobox = "";
	// 		string city = "";
	// 		cc();
	// 		cwl("Ooh! A new employee, huh? ｀;:゛;｀;･(°ε° )");
	// 		bool startover = true;
	// 		do
	// 		{
	// 			cwl("What is their name?");
	// 			cw("→");
	// 			name = crl();
	// 			cwl("Nice! What's their address? (street and house number)");
	// 			cw("→");
	// 			address = crl();
	// 			cwl("What's the PO-Box?");
	// 			cw("→");
	// 			pobox = crl();
	// 			cwl("What city is that?");
	// 			cw("→");
	// 			city = crl();
	// 			cwl($"What position will {name} have?");
	// 			Positions[] positions = (Positions[])Enum.GetValues(typeof(Positions));
	// 			for (int i = 0; i < positions.Length; i++)
	// 			{
	// 				Console.WriteLine($"{i}: {positions[i]}");// I know the limit will be from 0 to 9, but this is good enough for now
	// 			}
	// 			ccc();
	// 			bool correct = false;
	// 			do
	// 			{
	// 				ConsoleKeyInfo keyInfo = crk();
	// 				switch (keyInfo.KeyChar)
	// 				{
	// 					case '0':
	// 						position = Positions.CEO;
	// 						correct = true;
	// 						break;
	// 					case '1':
	// 						position = Positions.HR;
	// 						correct = true;
	// 						break;
	// 					case '2':
	// 						position = Positions.journalist;
	// 						correct = true;
	// 						break;
	// 					case '3':
	// 						position = Positions.marketing;
	// 						correct = true;
	// 						break;
	// 					case '4':
	// 						position = Positions.printer;
	// 						correct = true;
	// 						break;
	// 					default:
	// 						ccc();
	// 						cwl("That's not a valid option. ( ◡‿◡ *)");
	// 						break;
	// 				}
	// 			} while (!correct);
	// 			ccc();
	// 			cwl("Just a checkup, is this correct?");
	// 			cwl($"Name: {name}");
	// 			cwl($"Address: {address}");
	// 			cwl($"PO-Box: {pobox}");
	// 			cwl($"City: {city}");
	// 			cwl($"Position: {position}");
	// 			cwl("Y/N");
	// 			bool breakOut = false;
	// 			do
	// 			{
	// 				ConsoleKeyInfo keyInfo = crk();
	// 				ccc();
	// 				switch (keyInfo.Key)
	// 				{
	// 					case ConsoleKey.Y:
	// 						startover = false;
	// 						breakOut = true;
	// 						break;
	// 					case ConsoleKey.N:
	// 						startover = true;
	// 						breakOut = true;
	// 						cwl("Alright, starting over...");
	// 						zzz(2000);
	// 						ccc();
	// 						break;
	// 					default:
	// 						ccc();
	// 						cwl("I didn't quit catch that.");
	// 						break;
	// 				}
	// 			} while (!breakOut);
	// 			cc();
	// 			cwl($"Awesome! Tell {name} congratulations from me! (o^▽^o)");
	// 			zzz(2000);
	// 		} while (startover);

	// 		if(lastEmployeeID.Hex == "0"){
	// 			this.EmployeeID = lastEmployeeID;
	// 		}else{
	// 			this.EmployeeID = HexMath.HexIncrement(lastEmployeeID);
	// 		}
	// 		this.Name = name;
	// 		this.Address = address;
	// 		this.POBox = pobox;
	// 		this.City = city;
	// 		this.Position = position;
	// 		this.EmploymentDate = DateTime.Now;
	// 	} // End of NewEmployee
	// }
}