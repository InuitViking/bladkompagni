using System.Xml.Linq;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Bladkompagni.Converter;
using Bladkompagni.Classes;
using static Bladkompagni.Classes.Helpers;
namespace Bladkompagni
{
	public class Bladkompagniet
	{
		public string Name{get;set;}
		public string CVR{get;set;}
		public string Address{get;set;}
		public string POBox{get;set;}
		public string City{get;set;}
		public List<Menu> Menus = new List<Menu>();
		public List<Ansat> Employees = new List<Ansat>();
		public Hexadecimal LastMenuID = new Hexadecimal();
		public Hexadecimal LastEmployeeID = new Hexadecimal();

		/// <summary>
		/// Is used to for empty initialization, so the initialization part can run.
		/// </summary>
		public Bladkompagniet(){}

		/// <summary>
		/// You must give it 5 string parametres:
		/// 1. Name
		/// 2. Address
		/// 3. PO-Box
		/// 4. City
		/// 5. CVR
		/// </summary>
		/// <param name="name"></param>
		public Bladkompagniet(string name,string address, string pobox, string city, string cvr){
			this.Name = name;
			this.Address = address;
			this.POBox = pobox;
			this.City = city;
			this.CVR = GenerateCVR();
			this.LastMenuID.Hex = "0";
			this.LastEmployeeID.Hex = "0";
			Menu createMainMenu = new Menu(LastMenuID, "Main menu");
			Menus.Add(createMainMenu);
		}

		/// <summary>
		/// Generates a CVR number (Centrale Virksomhedsregister).
		/// Typically, such a number consists of eight integers.
		/// </summary>
		/// <returns>string</returns>
		public static string GenerateCVR(){
			Random random = new Random();
			StringBuilder CVR = new StringBuilder();
			for (int i = 0; i < 8; i++)
			{
				string randomNumber = Convert.ToString(random.Next(10));
				CVR.Append(randomNumber);
			}
			return CVR.ToString();
		}

		public void ListEmployees(){
			Console.WriteLine(this.Employees[0].EmployeeID.Hex);
			for (int i = 0; i < this.Employees.Count; i++)
			{
				Console.WriteLine(this.Employees[i].EmployeeID.Hex);
			}
			Console.WriteLine("Press any key to continue");
			crk();
		}
	}
}