using System;
using System.Threading;
namespace Bladkompagni.Classes
{
	public class Helpers
	{
		public static void cc() { Console.Clear(); }
		public static ConsoleKeyInfo crk() {
			return Console.ReadKey();
		}
		public static string crl(){
			return Console.ReadLine();
		}
		public static void zzz(int secs){
			Thread.Sleep(secs);
		}
		public static void ccc(){
			int currentLineCursor = Console.CursorTop;
			Console.SetCursorPosition(0, Console.CursorTop);
			Console.Write(new string(' ', Console.WindowWidth));
			Console.SetCursorPosition(0, currentLineCursor);
		}// End of ClearCurrentConsoleLine()
		public static void cwl(string text){
			char[] textChar = text.ToCharArray();
			for (int i = 0; i < textChar.Length; i++)
			{
				Console.Write(textChar[i]);
				zzz(50);
			}
			Console.WriteLine();
		}
		public static void cw(string text){
			char[] textChar = text.ToCharArray();
			for (int i = 0; i < textChar.Length; i++)
			{
				Console.Write(textChar[i]);
				zzz(50);
			}
			Console.Write(" ");
		}
	}
}