using System;
using System.ComponentModel;
using Bladkompagni.Converter;
using Bladkompagni.Classes;
using static Bladkompagni.Classes.Helpers;
namespace Bladkompagni.Classes
{
	public class Init
	{
		public static Bladkompagniet Initiate(){
			string bladkompagni="", address="", pobox="", city="", cvr="";
			cwl("Welcome to the program!");
			zzz(500);
			cwl("We first need to initiate your new magazine company, so I'm going to ask you a few questions, alright? :D");
			Console.WriteLine("1. Okay!");
			Console.WriteLine("2. No way!");
			ConsoleKeyInfo keyInfo;
			bool quit = false;
			bool one = false;
			bool two = false;
			do{
				keyInfo = crk();
				switch (keyInfo.KeyChar)
				{
					case '1':
						one = true;
						quit = true;
						break;
					case '2':
						two = true;
						quit = true;
						break;
					default:
						ccc();
						cwl("I didn't quite get that.");
						ccc();
						break;
				}
			}while(!quit);

			ccc();

			if(one){
				bool startOver = true;
				do
				{
					cwl("Awesome! What shall we call it?");
					cw("→");
					bladkompagni = crl();
					zzz(500);
					cw($"\"{bladkompagni}\"... ");
					zzz(1000);
					cwl("I like it! :o");
					cwl($"What's the road and number (address) of \"{bladkompagni}\"?");
					cw("→");
					address = crl();
					cwl("Awesome! What's the PO-Box?");
					cw("→");
					pobox = crl();
					cwl($"Neaterino! What city has the PO-Box {pobox}?");
					cw("→");
					city = crl();
					cwl("Fantastic! I'll go generate a CVR number for ya!");
					cwl("Generating... ☉ ‿ ⚆");
					zzz(2000);
					cvr = Bladkompagniet.GenerateCVR();
					cwl($"Here you go! (ﾉ☉ヮ⚆)ﾉ ⌒*:･ﾟ✧ {cvr}");
					cwl("Alright. I just need you to check... Are these info correct?");
					cwl($"Name: {bladkompagni}");
					cwl($"Address: {address}");
					cwl($"PO-Box: {pobox}");
					cwl($"City: {city}");
					Console.WriteLine("Y/N");
					bool breakOut = false;
					do
					{
						keyInfo = crk();
						ccc();
						switch (keyInfo.Key)
						{
							case ConsoleKey.Y:
								startOver = false;
								breakOut = true;
								break;
							case ConsoleKey.N:
								startOver = true;
								breakOut = true;
								cwl("Alright, starting over...");
								zzz(2000);
								ccc();
								break;
							default:
								ccc();
								cwl("I didn't quit catch that.");
								break;
						}
					} while (!breakOut);

					cc();
					cwl("Great! The initialization is done!");
					zzz(1000);
					cwl($"Good luck with {bladkompagni}!");
					zzz(2000);

				} while (startOver);
			}else if(two){
				cwl("Alright. I'll wait here. Closing program.");
				cc();
				Environment.Exit(0);
			}else{
				cwl("This wasn't supposed to happen...");
				Console.WriteLine("x_x *dead*");
				Environment.Exit(0);
			}

			Bladkompagniet newBladkompagni = new Bladkompagniet(bladkompagni, address, pobox, city, cvr);
			CreateMainMenuItems(newBladkompagni);
			return newBladkompagni;
		}// End of Initiate

		public static void Loader(){
			cc();
			cwl("Starting program...");
			for (int i = 0; i <= 100; i++)
			{
				string dot = "";
				cc();
				Console.WriteLine("Starting program...");
				Console.Write($"{i}% ");
				for(int j = 0; j < i; j++){
					dot += "█";
				}
				Console.WriteLine(dot);
				Random rand = new Random();
				zzz(rand.Next(50, 500));
			}
			cc();
			cwl("Hello!");
			zzz(500);
			cwl("Welcome back!");
			zzz(1000);
		}

		public static void CreateMainMenuItems(Bladkompagniet bladkompagni){
			// Create the "Create new employee" menu item
			MenuItem employeeItem = new MenuItem(bladkompagni.Menus[0].LastItemID,"Create New Employee");
			MenuItemFunc employeeFunc = new MenuItemFunc();
			employeeFunc.NewItem(employeeItem, new Ansat());
			bladkompagni.Menus[0].MenuItems.Add(employeeItem);

			// Create the "List employees" menu item
			MenuItem listEmpItem = new MenuItem(bladkompagni.Menus[0].LastItemID,"List Employees");
			MenuItemFunc listEmpItemFunc = new MenuItemFunc();
			listEmpItemFunc.NewItem(listEmpItem, bladkompagni);
			bladkompagni.Menus[0].MenuItems.Add(listEmpItem);

			// Create the "Quit" menu item
			MenuItem quitItem = new MenuItem(bladkompagni.Menus[0].LastItemID,"Quit");
			MenuItemFunc quitFunc = new MenuItemFunc();
			quitFunc.NewItem(quitItem, new Quit());
			bladkompagni.Menus[0].MenuItems.Add(quitItem);
		}
	}
}