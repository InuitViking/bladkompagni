using System.Collections.Generic;
using System;
using Bladkompagni.Converter;
namespace Bladkompagni.Classes
{
	public class Menu
	{
		public Hexadecimal MenuID{get;private set;}
		public string Name{get;set;}
		public List<MenuItem> MenuItems = new List<MenuItem>{};
		public Hexadecimal LastItemID = new Hexadecimal();
		public Menu(){}
		public Menu(Hexadecimal menuid, string name){
			if(menuid.Hex == "0"){
				this.MenuID = menuid;
			}else{
				menuid = HexMath.HexIncrement(menuid);
				this.MenuID.Hex = menuid.Hex;
			}
			this.Name = name;
			LastItemID.Hex = "0";
		}

	}
}