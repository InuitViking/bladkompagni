using System.Reflection.Metadata;
using System.Runtime.InteropServices;
using System;
using System.Reflection;
using Bladkompagni.Converter;
namespace Bladkompagni.Classes
{
	public class MenuItem : Menu
	{
		public Hexadecimal ItemID{get;private set;}
		public string ItemName{get;set;}
		public object Funcs{get;set;} // This is most likely illegal per-se, but I need it to hold objects.
		public MenuItem(Hexadecimal lastItemID, string name){
			if(lastItemID.Hex == "0"){
				this.ItemID = lastItemID;
			}else{
				this.ItemID = HexMath.HexIncrement(lastItemID);
			}
			this.ItemName = name;
		}
	}
}