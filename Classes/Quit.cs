using System;
using System.ComponentModel;
using Bladkompagni.Converter;
using Bladkompagni.Classes;
using static Bladkompagni.Classes.Helpers;
namespace Bladkompagni.Classes
{
	public class Quit
	{
		public void Exit(){
			cc();
			cwl("Are you sure you want to quit?");
			cwl("Since this program doesn't save anything, everything will be lost");
			cw("Y/N");
			bool exit = false;
			do
			{
				ConsoleKeyInfo keyInfo = crk();
				switch (keyInfo.Key)
				{
					case ConsoleKey.Y:
						ccc();
						cwl("Okay... 。゜゜(´Ｏ`) ゜゜。");
						cwl("Goodbye...");
						zzz(500);
						Environment.Exit(0);
						break;
					case ConsoleKey.N:
						ccc();
						cwl("Returning! (o^▽^o)");
						zzz(500);
						exit = true;
						break;
					default:
						ccc();
						cwl("Say what again?");
						break;
				}
			} while (!exit);
		}
	}
}