using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Json;
using System.Globalization;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Bladkompagni.Converter;
using static Bladkompagni.Converter.Hexadecimal;

namespace Bladkompagni.Converter
{
	public class ConvertHex
	{

		/// <summary>
		/// Converts hexadecimal to decimal.
		/// Based on this: https://www.rapidtables.com/convert/number/hex-to-decimal.html
		/// </summary>
		/// <param name="b16n"></param>
		/// <returns>double</returns>
		public static double HexToDouble(string b16n){
			char[] hexTable = new char[16]{'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
			string[] parts = b16n.Split('.');
			char[] hexCarry = parts[0].ToCharArray();
			char[] hexCarryReverse = hexCarry.Reverse().ToArray();
			char[] hexFraction = parts[1].ToCharArray();
			List<double> carryMathList = new List<double>();
			List<double> fractionMathList = new List<double>();

			for (int i = 0; i < hexCarry.Length; i++)
			{
				int pos = Array.BinarySearch(hexTable, hexCarryReverse[i]);
				carryMathList.Add((pos)*(Math.Pow(16,i)));
			}
			for (int i = 0; i < hexFraction.Length; i++)
			{
				int pos = Array.BinarySearch(hexTable, hexFraction[i]);
				fractionMathList.Add((pos)*(Math.Pow(16,-i-1)));
			}

			return (carryMathList.Sum() + fractionMathList.Sum());
		}

		/// <summary>
		/// Converts a double to Hexadecimal
		/// </summary>
		/// <param name="b10n"></param>
		/// <returns>Hexadecimal</returns>
		public static Hexadecimal DoubleToHex(double b10n){
			string s = b10n.ToString("0.0000", CultureInfo.InvariantCulture);
			string[] parts = s.Split('.');
			int carry = Convert.ToInt32(parts[0]);
			Hexadecimal hexFraction = DFrac(Convert.ToDouble("0."+parts[1]));
			Hexadecimal hexadecimal = new Hexadecimal();
			hexadecimal.Hex = carry.ToString("X");
			hexadecimal.Hex += "."+hexFraction.Hex;
			return hexadecimal;
		}

		/// <summary>
		/// This takes a decimal fraction and converts it to a hexadecimal fraction.
		/// Default decimal numbers (numbers after decimal point) is 4.
		/// It returns an object (Hexadecimal) but should be seen merely as a "type".
		/// b10n=Base 10 Number
		/// This method is based on this (unfortunately now dead somehow):
		/// https://www.schoolelectronic.com/2013/09/convert-decimal-fraction-to-hexadecimal.html
		/// </summary>
		/// <param name="b10n"></param>
		/// <returns>Hexadecimal</returns>
		public static Hexadecimal DFrac(double b10n){
			/*
				Add 1, because we need to be able to select from the list below.
				Besides, this is only meant for the fractional part of a number.
			*/
			if(b10n < 0){
				b10n = b10n+1;
			}
			// New fraction in hex
			Hexadecimal fractionHex = new Hexadecimal();
			fractionHex.Hex = "";

			if(b10n >= 1 || b10n <= -1){
				throw new Exception($"Must be below 1 or over -1! \"{b10n}\" used");
			}else{
				// List containing hexnumbers
				List<string> hexTable = new List<string>{"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
				// number of iterations
				int iterations = 4;
				// result with decimal point
				double product = 0.0000;

				// integer before decimal point
				int carry = 0;
				// ...in hex
				Hexadecimal carryHex = new Hexadecimal();
				carryHex.Hex = "";

				double newDouble = 0.0000;

				for (int i = 0; i < iterations; i++){
					if(i==0){
						product = b10n*16;
					}else{
						product = newDouble*16;
					}
					string s = product.ToString("0.0000", CultureInfo.InvariantCulture);
					string[] parts = s.Split('.');
					carry = Convert.ToInt32(parts[0]);
					carryHex.Hex = hexTable[carry];
					newDouble = Convert.ToDouble(("0." + parts[1]));
					fractionHex.Hex += carryHex.Hex;
				}
				return fractionHex;
			}
		}
	}
}
