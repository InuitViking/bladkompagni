using System.Security.AccessControl;
using System.Linq;
using System;
using System.Collections.Generic;
namespace Bladkompagni.Converter
{
	public class HexMath
	{
		public static Hexadecimal HexIncrement(Hexadecimal hex){
			// "Table" containing hexadecimal values. Used for the index value.
			char[] hexTableChar = new char[]{'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
			// string[] hexTable = new string[]{"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
			// hexCharList is used to put the individual parts of the string into chars in a list
			List<char> hexCharList = new List<char>{};
			// hexCharListReverse is the same, but in reverse to "easier do math"
			List<char> hexCharListReverse = new List<char>{};

			// hexCharArray to put the things into an array, because it's easier to put in an array, and later in a list
			char[] hexCharArray = hex.Hex.ToCharArray();
			char[] newHexCharArray = new char[]{};

			// Will contain the positional index from hexTable depending on the values it contains.
			int pos = 0;

			// Puts the hexCharArray into hexCharList
			for (int i = 0; i < hexCharArray.Length; i++)
			{
				hexCharList.Add(hexCharArray[i]);
			}

			// Reversively puts the hexCharArray into hexCharListReverse.
			// using hexCharArray.Reverse() would result in IEnumerable errors. This musn't happen as we need them indexed
			for (int i = hexCharArray.Length-1; i >= 0; i--)
			{
				hexCharListReverse.Add(hexCharArray[i]);
			}

			bool shouldNextIncrement = false;

			if(hexCharListReverse.Count == 1){
				if(hexCharListReverse[0] == hexTableChar[15]){
					hexCharListReverse.Add('1');
					hexCharListReverse[0] = hexTableChar[0];
				}else{
					pos = Array.BinarySearch(hexTableChar, hexCharListReverse[0]);
					hexCharListReverse[0] = hexTableChar[pos+1];
				}
			}else{
				if (hexCharListReverse.All(o => o == hexTableChar[15])){
					hexCharListReverse = hexCharListReverse.Select (x => '0') .ToList();
					hexCharListReverse.Add('1');
				}else{
					int i;
					for (i=0;i < hexCharListReverse.Count; i++)
					{
						pos = Array.BinarySearch(hexTableChar, hexCharListReverse[i]);
						if(shouldNextIncrement){

							if(hexCharListReverse[i] == hexTableChar[15]){
								shouldNextIncrement = true;
								hexCharListReverse[i] = hexTableChar[0];
							}else if(hexCharListReverse[i] != hexTableChar[15]){
								hexCharListReverse[i] = hexTableChar[pos+1];
								shouldNextIncrement = false;
								break;
							}
						}else{
							if(hexCharListReverse[i] == hexTableChar[15]){
								shouldNextIncrement = true;
								hexCharListReverse[i] = hexTableChar[0];
							}else if(hexCharListReverse[i] != hexTableChar[15]){
								hexCharListReverse[i] = hexTableChar[pos+1];
								shouldNextIncrement = false;
								break;
							}
						}
					}
				}
			}

			List<char> newHexCharList = new List<char>{};
			for (int i = hexCharListReverse.Count-1; i >= 0; i--)
			{
				newHexCharList.Add(hexCharListReverse[i]);
			}

			// Create a new string out of hexCharArray (so it can be put into hex.Hex)
			hex.Hex = new string(newHexCharList.ToArray());
			// Return our incremented hexadecimal
			return hex;
		}
	}
}
