using Bladkompagni.Classes;
namespace Bladkompagni.Interfaces
{
	public interface IMenuItem<T>
	{
		void NewItem(MenuItem menuItem, T funcs);
	}
}