﻿using System.ComponentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using Bladkompagni.Converter;
using static Bladkompagni.Converter.ConvertHex;
using static Bladkompagni.Converter.HexMath;
using static Bladkompagni.Classes.Helpers;
using Bladkompagni.Classes;

namespace Bladkompagni
{
	class Program
	{
		static void Main(string[] args)
		{
			bool run = true;
			bool init = false;
			Bladkompagniet bladkompagni =  new Bladkompagniet();
			cc();
			do{

				if(!init){
					bladkompagni = Init.Initiate();
					init = true;
				}else if(init){
					Init.Loader();
					do{
						cc();
						cwl($"{bladkompagni.Menus[0].Name}:");
						cwl($"E: {bladkompagni.Menus[0].MenuItems[0].ItemName}");
						cwl($"L: {bladkompagni.Menus[0].MenuItems[1].ItemName}");
						cwl($"Q: {bladkompagni.Menus[0].MenuItems[bladkompagni.Menus[0].MenuItems.Count -1].ItemName}");
						bool menuRun = false;
						do
						{
							ConsoleKeyInfo keyInfo = crk();
							switch (keyInfo.Key)
							{
								case ConsoleKey.E:
									ccc();
									Ansat employee = (Ansat) bladkompagni.Menus[0].MenuItems[0].Funcs;
									employee.NewEmployee(bladkompagni.LastEmployeeID, bladkompagni);
									if(bladkompagni.LastEmployeeID.Hex == "0" && bladkompagni.Employees.Count == 0){
										// nothing
										Console.WriteLine(bladkompagni.LastEmployeeID.Hex);
									}else{
										bladkompagni.LastEmployeeID = HexMath.HexIncrement(bladkompagni.LastEmployeeID);
										Console.WriteLine(bladkompagni.LastEmployeeID.Hex);
									}



									bladkompagni.Employees.Add(employee);
									break;
								case ConsoleKey.L:
									cc();
									Bladkompagniet listEmployees = (Bladkompagniet) bladkompagni.Menus[0].MenuItems[1].Funcs;
									// listEmployees.Employees(bladkompagni.Employees);
									listEmployees.ListEmployees();
									break;
								case ConsoleKey.Q:
									ccc();
									Quit exit = (Quit) bladkompagni.Menus[0].MenuItems[bladkompagni.Menus[0].MenuItems.Count -1].Funcs;
									exit.Exit();
									break;
								default:
									ccc();
									cwl("Didn't quite catch that");
									break;
							}
						} while (menuRun);
					}while(true);
				}

			}while(run);
			Environment.Exit(0);
		}
	}
}
